//
// Created by lili on 18-12-5.
//

#include <stdio.h>
#include <string.h>
#include <cutils/uevent.h>
#include <malloc.h>

#include "vendor.h"
#include "property_service.h"
#include "devices.h"
#include "ueventd_parser.h"
#include "util.h"
#include "log.h"

#define MAX_DEVICE_NUM 8
#define SD_DEV_NAME   "fe320000.dwmmc"
#define USB_DEV_NAME  "fe3c0000.usb"
#define EMMC_DEV_NAME "fe330000.sdhci"
#define NVME_DEV_NAME "f8000000.pcie"

/**
 * copy form device.cpp
 */
struct uevent {
    const char *action;
    const char *path;
    const char *subsystem;
    const char *firmware;
    const char *partition_name;
    const char *device_name;
    int partition_num;
    int major;
    int minor;
};

char* device_list[MAX_DEVICE_NUM][2] = {};

int save_find_block_device(char* name){

    for (int i = 0; i < MAX_DEVICE_NUM; i++){
        if(!device_list[i][0]){
            if(asprintf(&device_list[i][1], "/dev/block/by-label%d", i) > 0){
                if(asprintf(&device_list[i][0], "%s", name) > 0){
                    return i;
                }
                ERROR("device_list:asprintf error!\n");
            }
        }else if(!strcmp(name, device_list[i][0])){
            return i;
        }
    }

    WARNING("device:%s not save!\n", name);
    return -1;
}

void do_major_block(int device_num, char** links, int* num){

    if(asprintf(&links[*num], "%s/%s", device_list[device_num][1], "device_main") > 0){
        *num += 1;
    }else{
        links[*num] = NULL;
    }
}

void vendor_get_block_device_symlinks(struct uevent *uevent, char* device_name, char** links, int* num){

    int device_num = save_find_block_device(device_name);
    if(device_num < 0) return;

    if(!uevent -> partition_name && uevent ->partition_num < 0){
        do_major_block(device_num, links, num);
        return;
    }

    if(uevent -> partition_name){
        char* p = strdup(uevent->partition_name);
        sanitize(p);
        if (strcmp(uevent->partition_name, p) > 0)
            INFO("Linking partition '%s' as '%s'\n", uevent->partition_name, p);
        if (asprintf(&links[*num], "%s/%s", device_list[device_num][1], p) > 0)
            (*num) ++;
        else
            links[*num] = NULL;
        free(p);
    }
}

void vendor_init(){

    std::string value = property_get("ro.boot.mode");
    std::string devices = EMMC_DEV_NAME;
    if(!value.empty()){
        if(value == "sd"){
            devices = SD_DEV_NAME;
        } else if(value == "usb"){
            devices = USB_DEV_NAME;
        } else if(value == "nvme"){
            devices = NVME_DEV_NAME;
        }
    }
    INFO("vendor_init:value=%s\n", value.c_str());
    INFO("vendor_init:devices=%s\n", devices.c_str());
    if(asprintf(&device_list[0][1], "%s", "/dev/block/by-label0") > 0){
        if(!asprintf(&device_list[0][0], "%s", devices.c_str())){
            ERROR("vendor_init: asprintf error");
        }
    }
}